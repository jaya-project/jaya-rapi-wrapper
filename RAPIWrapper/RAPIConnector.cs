﻿using OpenNETCF.Desktop.Communication;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net;
using System.Text.RegularExpressions;

namespace RAPIWrapper
{
    public class RAPIConnector
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RAPIConnector));

        public bool RemoveFileAfterCopy
        {
            set;
            get;
        }

        public enum Status : int
        {
            Error = -1, Success = 1, DeviceNotFound = 2, DeviceNotConnected = 3,
            FileNotFound = 4, ExistingFile = 5
        }

        public int StatusCode
        {
            get;
            set;
        }

        public bool OverwriteFile
        {
            get;
            set;
        }

        private RAPI Device;

        public RAPIConnector()
        {
            Device = new RAPI();
            StatusCode = (int)Status.Success;
        }

        /// <summary>
        /// Copies a list of files between the device and desktop pc.
        /// </summary>
        /// <param name="source">
        /// Source forlder/file to copy.
        /// </param>
        /// <param name="destination">
        /// Destination folder/file to copy to.
        /// </param>
        /// <param name="files">
        /// List of files to copy.
        /// </param>
        /// <param name="toDevice">If set to true, it will take </param>
        public void CopyFiles(String source, String destination, String[] files, bool toDevice)
        {
            if (Connect())
            {
                if (source != null && source.Length > 0 &&
                    destination != null && destination.Length > 0 &&
                    files != null && files.Length > 0)
                {
                    IterateFiles(source, destination, files, toDevice);
                }

                Disconnect();
            }
        }

        private void IterateFiles(String source, String destination, String[] files, bool copyToDevice)
        {
            foreach (String file in files)
            {
                String remote = "",
                    local = "";

                if (copyToDevice)
                {
                    local = Path.Combine(source, Path.GetFileName(file));
                    remote = Path.Combine(destination, Path.GetFileName(file));
                    log.DebugFormat("IterateFiles - String[]: [remote={0}, local={1}].", remote, local);

                    if (IsDesktopDirectory(local))
                    {
                        IterateFiles(local,
                            remote,
                            Directory.EnumerateFiles(local, "*", SearchOption.AllDirectories).ToArray(),
                            copyToDevice);
                    }
                    else
                    {
                        CopyFileToDevice(local, remote);
                    }
                }
                else
                {
                    local = Path.Combine(destination, file);
                    remote = Path.Combine(source, file);
                    log.DebugFormat("IterateFiles - String[]: [remote={0}, local={1}].", remote, local);

                    if (IsDirectory(remote))
                    {
                        IterateFiles(remote,
                            local,
                            Device.EnumerateFiles(Path.Combine(remote, "*")),
                            copyToDevice);
                    }
                    else
                    {
                        CopyFileToDesktop(remote, local);
                    }
                }
            }
        }

        private void IterateFiles(String source, String destination, IEnumerable<FileInformation> files, bool copyToDevice)
        {
            foreach (FileInformation finfo in files)
            {
                String remote = "",
                    local = "";

                if (copyToDevice)
                {
                    local = Path.Combine(source, finfo.FileName);
                    remote = Path.Combine(destination, finfo.FileName);
                    log.DebugFormat("IterateFiles - IEnumerable: [remote={0}, local={1}].", remote, local);

                    if (IsDesktopDirectory(local))
                    {
                        IterateFiles(local,
                            remote,
                            Directory.EnumerateFiles(local, "*", SearchOption.AllDirectories).ToArray(),
                            copyToDevice);
                    }
                    else
                    {
                        CopyFileToDevice(local, remote);
                    }
                }
                else
                {
                    local = Path.Combine(destination, finfo.FileName);
                    remote = Path.Combine(source, finfo.FileName);
                    log.DebugFormat("IterateFiles - IEnumerable: [remote={0}, local={1}].", remote, local);

                    if (IsDirectory(remote))
                    {
                        IterateFiles(local,
                            remote,
                            Device.EnumerateFiles(Path.Combine(remote, "*")),
                            copyToDevice);
                    }
                    else
                    {
                        CopyFileToDesktop(remote, local);
                    }
                }
            }
        }

        private void CopyFileToDevice(String desktopFile, String deviceFile)
        {
            if (Device.Connected)
            {
                if (File.Exists(desktopFile))
                {
                    try
                    {
                        log.DebugFormat("Copying file from DESKTOP [{0}] to DEVICE [{1}]...", desktopFile, deviceFile);
                        String tmp = Path.GetDirectoryName(deviceFile);

                        if (!Device.DeviceFileExists(tmp))
                        {
                            if (!CreateDirectory(tmp))
                            {
                                return;
                            }
                        }

                        Device.CopyFileToDevice(desktopFile, deviceFile, OverwriteFile);
                    }
                    catch (Exception ex)
                    {
                        log.ErrorFormat("Failed to copy file [DESKTOP file={0}] to [DEVICE file={1}]: {2}", desktopFile, deviceFile, ex);
                        StatusCode = (int)Status.Error;
                    }
                }
                else
                {
                    log.ErrorFormat("File [{0}] does not exist on desktop.", desktopFile);
                    StatusCode = (int)Status.FileNotFound;
                }
            }
            else
            {
                log.ErrorFormat("Can't copy file [{0}] to device: device disconnected.", desktopFile);
                StatusCode = (int)Status.DeviceNotConnected;
            }
        }

        private void CopyFileToDesktop(String deviceFile, String desktopFile)
        {
            if (Device.Connected)
            {
                if (Device.DeviceFileExists(deviceFile))
                {
                    if (IsValidFile(Device.GetDeviceFileAttributes(deviceFile)))
                    {
                        log.DebugFormat("Copying file from DEVICE [{0}] to DESKTOP [{1}]...", desktopFile, deviceFile);

                        try
                        {
                            String tmp = Path.GetDirectoryName(desktopFile);

                            if (!Directory.Exists(tmp))
                                Directory.CreateDirectory(tmp);

                            if (!OverwriteFile)
                            {
                                if (!File.Exists(desktopFile))
                                    Device.CopyFileFromDevice(desktopFile, deviceFile, false);
                                else
                                {
                                    log.InfoFormat("The file {0} already exists and the overwrite option was set to false.", desktopFile);
                                    StatusCode = (int)Status.ExistingFile;
                                }
                            }
                            else
                            {
                                Device.CopyFileFromDevice(desktopFile, deviceFile, true);
                            }

                            if (RemoveFileAfterCopy)
                                RemoveFile(deviceFile);
                        }
                        catch (Exception ex)
                        {
                            log.ErrorFormat("Failed to copy file [DEVICE file={1}] to [DESKTOP file={0}]: {2}", desktopFile, deviceFile, ex);
                            StatusCode = (int)Status.Error;
                        }
                    }
                }
                else
                {
                    log.ErrorFormat("File [{0}] does not exist on the device.", deviceFile);
                    StatusCode = (int)Status.FileNotFound;
                }
            }
            else
            {
                log.ErrorFormat("Can't copy file [{0}] to desktop: device disconnected.", deviceFile);
                StatusCode = (int)Status.DeviceNotConnected;
            }
        }

        public void RemoveFile(String[] files)
        {
            if (files != null && files.Length > 0)
            {
                foreach (String file in files)
                {
                    RemoveFile(file);
                }
            }
            else
            {
                log.Info("Tried to remove files but the array was either null or empty.");
                StatusCode = (int)Status.Error;
            }
        }

        public void RemoveFile(String file)
        {
            if (Device.Connected)
            {
                if (IsValidFile(Device.GetDeviceFileAttributes(file)))
                {
                    try
                    {
                        Device.DeleteDeviceFile(file);
                    }
                    catch (Exception ex)
                    {
                        log.ErrorFormat("Failed to delete file {0}: {1}", file, ex);
                        StatusCode = (int)Status.Error;
                    }
                }
                else
                {
                    log.WarnFormat("The file {0} is not a valid file.", file);
                    StatusCode = (int)Status.Error;
                }
            }
            else
            {
                log.ErrorFormat("Can't remove file {0}: device disconnected.", file);
                StatusCode = (int)Status.DeviceNotConnected;
            }
        }

        private bool CreateDirectory(String path)
        {
            log.InfoFormat("Trying to create Device directory [{0}].", path);
            try
            {
                Device.CreateDeviceDirectory(path);
                return true;
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Failed to create directory [{0}]: {1}", path, ex);
            }

            return false;
        }

        private bool IsDirectory(String path)
        {
            try
            {
                return Device.GetDeviceFileAttributes(path) == RAPI.RAPIFileAttributes.Directory;
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Is Directory [{0}] failed: {1}", path, ex);
            }

            return false;
        }

        private bool IsDirectory(FileInformation finfo)
        {
            try
            {
                return finfo.FileAttributes == (int)RAPI.RAPIFileAttributes.Directory;
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Is Directory [{0}] failed: {1}", finfo.FileName, ex);
            }

            return false;
        }

        private bool IsDesktopDirectory(String path)
        {
            log.DebugFormat("Is desktop directory: {0}", path);

            try
            {
                FileAttributes atts = File.GetAttributes(path);
                return (atts & FileAttributes.Directory) == FileAttributes.Directory;
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Is Directory failed: {0}", ex);
            }

            return false;
        }

        private bool IsValidFile(FileInformation finfo)
        {
            return finfo.FileAttributes != (int)RAPI.RAPIFileAttributes.InROM &&
                finfo.FileAttributes != (int)RAPI.RAPIFileAttributes.ROMModule &&
                finfo.FileAttributes != (int)RAPI.RAPIFileAttributes.System &&
                finfo.FileAttributes != (int)RAPI.RAPIFileAttributes.Temporary;
        }

        private bool IsValidFile(RAPI.RAPIFileAttributes atts)
        {
            return atts != RAPI.RAPIFileAttributes.InROM &&
                atts != RAPI.RAPIFileAttributes.ROMModule &&
                atts != RAPI.RAPIFileAttributes.System &&
                atts != RAPI.RAPIFileAttributes.Temporary;
        }

        private void Disconnect()
        {
            if (Device != null && Device.Connected)
            {
                Device.Disconnect();
            }
        }

        private bool Connect()
        {
            bool r = false;

            try
            {
                if (Device.DevicePresent)
                {
                    log.Info("Device is present.");
                    Device.Connect();

                    if (Device.Connected)
                    {
                        r = true;
                        log.Info("Device connected.");
                    }
                    else
                    {
                        log.Error("Failed to connect to the device.");
                        StatusCode = (int)Status.Error;
                    }
                }
                else
                {
                    log.Warn("Could not connect to device: Device not present.");
                    StatusCode = (int)Status.DeviceNotFound;
                }
            }
            catch (Exception ex)
            {
                log.Error("Connect - Exception caught: {0}", ex);
            }

            return r;
        }

        /// <summary>
        /// Checks if the device is present or not.
        /// </summary>
        public void CheckStatus()
        {
            if (Device.DevicePresent)
            {
                StatusCode = (int)Status.Success;
            }
            else
            {
                StatusCode = (int)Status.DeviceNotFound;
            }
        }
    }
}
