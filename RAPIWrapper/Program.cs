﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NDesk.Options;
using System.IO;
using log4net;
using log4net.Config;
using System.Text.RegularExpressions;

namespace RAPIWrapper
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        public String ConfigPath { get; set; }
        public String SourceFolder { get; set; }
        public String DestinationFolder { get; set; }
        private String RemoteFileList { get; set; }
        private String LocalFileList { get; set; }
        private String RemoveFiles { get; set; }
        private RAPIConnector Connector = null;
        private bool Help = false;
        private bool RemoveAfterCopy = false;
        private bool Overwrite = false;
        private bool Test = false;
        private bool CheckStatus = false;

        public enum CopyType
        {
            Remote, Local
        }

        private int Start(string[] args)
        {
            ParseArguments(args);
            ConfigureLogger();
            log.InfoFormat("Arguments: {0}", args);

            if (Help)
            {
                PrintHelp();
            }
            else if (Test)
            {
                T();
            }
            else
            {
                Execute();

                if (Connector != null)
                    return Connector.StatusCode;
                else
                    return -1;
            }

            return 0;
        }

        private void Execute()
        {
            log.Info("--- APPLICATION START ---");
            Connector = new RAPIConnector();

            if (!CheckStatus)
            {
                if (SourceFolder != null && SourceFolder.Length > 0 &&
                    DestinationFolder != null && DestinationFolder.Length > 0)
                {
                    try
                    {
                        Connector.RemoveFileAfterCopy = RemoveAfterCopy;
                        Connector.OverwriteFile = Overwrite;

                        if (LocalFileList != null && LocalFileList.Length > 0)
                        {
                            log.InfoFormat("Trying to copy from DESKTOP [source={0}] to DEVICE [destination={1}].", SourceFolder, DestinationFolder);
                            Connector.CopyFiles(SourceFolder, DestinationFolder, LocalFileList.Split(','), true);
                        }
                        else if (RemoteFileList != null && RemoteFileList.Length > 0)
                        {
                            log.InfoFormat("Trying to copy from DEVICE [source={0}] to DESKTOP [destination={1}].", SourceFolder, DestinationFolder);
                            Connector.CopyFiles(SourceFolder, DestinationFolder, RemoteFileList.Split(','), false);
                        }
                        else if (RemoveFiles != null && RemoveFiles.Length > 0)
                        {
                            String[] flist = RemoveFiles.Split(',');
                            log.DebugFormat("Trying to delete {0} files.", flist.Length);
                            Connector.RemoveFile(flist);
                        }
                        else
                        {
                            log.Error("No file list specified.");
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("RAPIWrapper Execution failed: ", ex);
                    }
                }
                else
                {
                    log.ErrorFormat("Source and/or destination folders were empty or null: {0} - {1}.", SourceFolder, DestinationFolder);
                }
            }
            else
            {
                Connector.CheckStatus();
            }

            log.Info("--- APPLICATION END ---");
        }

        private void ParseArguments(string[] args)
        {
            OptionSet options = new OptionSet()
            {
                { "h|help", val => Help = (val != null) },
                { "st|status", val => CheckStatus = (val != null) },
                { "c|confPath=", val => ConfigPath = val },
                { "sf|srcFolder=", val => SourceFolder = val },
                { "df|desFolder=", val => DestinationFolder = val },
                { "copyToDevice=", val => LocalFileList = val },
                { "copyToDesktop=", val => RemoteFileList = val },
                { "r|removeAfterCopy", val => RemoveAfterCopy = (val != null) },
                { "rm=", val => RemoveFiles = val },
                { "o|overwrite", val => Overwrite = (val != null) },
                { "test", val => Test = (val != null) }
            };

            List<String> arguments = options.Parse(args);
        }

        private void ConfigureLogger()
        {
            try
            {
                if (ConfigPath.Length > 0)
                {
                    FileInfo file = new FileInfo(ConfigPath);

                    if (file.Exists)
                        XmlConfigurator.Configure(file);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Could not configure logger: {0}.", ex);
            }
        }

        private void PrintHelp()
        {
            Console.WriteLine("\nConnects to a device using OpenRAPI and executes some commands on it.\n\n[Usage]\n");
            Console.WriteLine("-st,--status ...................... Checks if the device is present.");
            Console.WriteLine("-h,--help ......................... Prints this help.\n");
            Console.WriteLine("-c path,--confPath=path ........... Path to the log4net configuration file.\n");
            Console.WriteLine("-sf path,--srcFolder=path ......... Source directory where to copy the files from.\n");
            Console.WriteLine("-df path,--desFolder=path ......... Destination folder where to place the files.\n");
            Console.WriteLine("-copyToDevice=files ............... Copies a file or folder from DESKTOP to DEVICE\n");
            Console.WriteLine("-copyToDesktop=files .............. Copies a file or folder from DEVICE to DESKTOP\n");
            Console.WriteLine("-r,--removeAfterCopy .............. Indicates whether or not the copied files should be removed after copy. Only applies when files are copied from device to desktop.\n");
            Console.WriteLine("-rm=file_list ..................... Removes the files specified in the list (comma separated) from the DEVICE. Does not work with folders.");
            Console.WriteLine("-o,--overwrite .................... Overwrite files if they exists.");
        }

        private void T()
        {
            IEnumerable<String> files = Directory.EnumerateFiles("D:\\TestFolder\\logs", "*", SearchOption.AllDirectories);

            foreach (String s in files)
            {
                Console.WriteLine("File: {0}", s);
            }
        }

        /// <summary>
        /// Connects to a device using OpenRAPI and executes some commands on it.
        /// </summary>
        /// <param name="args">Parameters used by the application</param>
        /// <returns>
        /// One of the following status codes:
        /// Error = -1,
        /// Nothing Done = 0,
        /// Success = 1,
        /// DeviceNotFound = 2,
        /// DeviceNotConnected = 3,
        /// FileNotFound = 4.
        /// </returns>
        static int Main(string[] args)
        {
            return new Program().Start(args);
        }
    }
}
